package com.ingrammicro.ux1.backend;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.contrib.json.classic.JsonLayout;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

public class CustomJsonLayout extends JsonLayout {
    @Override
    protected void addCustomDataToJsonMap(Map<String, Object> map, ILoggingEvent iLoggingEvent) {
        try {
            map.put("hostname", InetAddress.getLocalHost().getHostName());
        } catch (UnknownHostException e) {
            //do nothing
        }
    }
}
