package com.ingrammicro.ux1.backend;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import javax.xml.crypto.Data;
import java.util.Random;

@Component
public class LogJob extends QuartzJobBean {
    private static final Logger logger = LoggerFactory.getLogger(LogJob.class);

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Random random = new Random();
        int n = random.ints(0, 5).findFirst().getAsInt();
        switch (n) {
            case 0:
                logger.trace("Trace log example");
                break;
            case 1:
                logger.debug("Debug log example");
                break;
            case 2:
                logger.info("Info log example");
                break;
            case 3:
                logger.warn("Warn log example");
                break;
            case 4:
                try {
                    throw new RuntimeException("Test exception");
                } catch (Exception e) {
                    logger.error("Exception log example", e);
                }
                break;
        }
    }
}
